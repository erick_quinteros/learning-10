import numpy as np
from itertools import combinations, chain


def intersection_area(d, r):
    """
    Return the area of intersection of the two cirlces of radius of r, and their centers are separated by d
    d, r: float
    """
    if d < 0:
        raise('the distance between two circles cannot be less than 0')
    elif np.isnan(r):
        # no visit
        return np.nan
    elif np.isnan(d):
        # has visit but no predict
        return 0
    elif d == 0:
        # complete overlap
        return np.pi * (r**2)
    elif d >= 2*r:
        # no overlap
        return 0
    else:
        r2 = r**2
        alpha = np.arccos(0.5*d/r)
        area = 2*alpha*r2 - r2*np.sin(2*alpha)
        return area


def haversine_numpy(arr):
    """
    calculate the distance between two points given latitude and longitude point1(lat1,lon1) and point2(lat2,lon2)
    arr: np_array or list having column lat1, lon1, lat2, lon2
    """
    # make sure it is 2d array
    arr = np.atleast_2d(arr)
    # earth radius in km
    radius = 6378.1
    # covert decimal degress to radians
    arr = np.radians(arr)
    # haversine formula
    dlat = arr[:,2]-arr[:,0]
    dlon = arr[:,3]-arr[:,1]
    a = np.sin(dlat/2)**2 + np.cos(arr[:,0]) * np.cos(arr[:,2]) * np.sin(dlon/2)**2
    c = 2 * np.arcsin(np.sqrt(a))
    d_km = 6371* c
    return d_km


def lat_lon_centroid(arr):
    """
    find the centroid given latitude and longitude of the points
    arr: np_array in [[lat1,lon1],[lat2,lon2],...]

    Original formula:
    1. Convert lat/lon (must be in radians) to Cartesian coordinates for each location.
    X = cos(lat) * cos(lon)
    Y = cos(lat) * sin(lon)
    Z = sin(lat)
    2. Compute average x, y and z coordinates.
    x = (x1 + x2 + ... + xn) / n
    y = (y1 + y2 + ... + yn) / n
    z = (z1 + z2 + ... + zn) / n
    3. Convert average x, y, z coordinate to latitude and longitude.
    Lon = atan2(y, x)
    Hyp = sqrt(x * x + y * y)
    Lat = atan2(z, hyp)
    """
    # make sure it is 2d array
    arr = np.atleast_2d(arr)
    if np.unique(arr, axis=0).shape[0] == 1:
        return arr[0,0], arr[0,1]
    else:
        # drop nan if any
        arr = arr[~(np.isnan(arr[:,0])|np.isnan(arr[:,1])),:]
        # covert decimal degress to radians
        arr = np.radians(arr)
        X = np.cos(arr[:,0]) * np.cos(arr[:,1])
        Y = np.cos(arr[:,0]) * np.sin(arr[:,1])
        Z = np.sin(arr[:,0])
        x = np.nanmean(X)
        y = np.nanmean(Y)
        z = np.nanmean(Z)
        return np.degrees(np.arctan2(z, np.sqrt(x * x + y * y))), np.degrees(np.arctan2(y, x))


def reject_outliers(arr,m=2):
    """
    reject outliers from a array that is m std away from the mean
    return the cleaned the array, and logical index
    """
    # keep nan to avoid error later
    ind_na = np.isnan(arr)
    ind = abs(arr - np.nanmean(arr))[~ind_na] <= m * np.nanstd(arr)
    return arr[ind|ind_na], ind|ind_na


def get_max_distance_between_n_points(points):
    """
    point should be in the format of [(lat1,lon1),(lat2,lon2),...]
    """
    if len(points) <1:
        return np.nan
    elif len(points) <2:
        return 0
    else:
        calculate_matrix = []
        for two_point in combinations(points,2):
            calculate_matrix.append(list(chain.from_iterable(two_point)))
        distance_matrix = np.array(calculate_matrix)
        return np.nanmax(haversine_numpy(distance_matrix))