####################################################################################################################
#
#
# aktana- engagement estimates estimates Aktana Learning Engines.
#
# description: estimate the target engagement probabilities
#
# created by : marc.cohen@aktana.com
# updated by : wendong.zhu@aktana.com
#
# updated on : 2019-03-12
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################

# helper function to estimate the likelihood for a visit based on the ECDF function fn
#E <- function(fn,x) return(fn$estimate[which(abs(fn$eval.points-x)==min(abs(fn$eval.points-x)))])

# main entry into the estimation function
calculateEngagement <- function(sc, chl, useForProbability, interactions, suggestions)
{
    library(ks)
    library(data.table)
    library(foreach)
    library(doMC)
    library(sparklyr)
    library(dplyr)

    registerDoMC(numberCores)  # setup the number of cores to use
    
    flog.info("calculateEngagement for channel %s ...", chl)

    events <- interactions %>% filter(repActionTypeId == chl & date <= today)

    events <- events %>%
      distinct(accountId, repId, date) %>%
      mutate(repActionTypeId=chl, reaction="Touchpoint")  # add back the two columns dropped due to distinct
    flog.info("finish initial prepare events")

    suggestions <- suggestions %>% filter(repActionTypeId==chl & date<=today & date>=sugStartDate)

    suggestions <- dplyr::rename(suggestions, suggestion = Suggestion_External_Id_vod__c)

    # remove the prefix AKTSUG~ from the suggestions
    suggestions <- suggestions %>% mutate(suggestion = regexp_replace(suggestion, "AKTSUG~", ""))
    flog.info("finish prepare suggestions")

    # identify the Trigger driven suggestions
    sugs <- suggestions %>% filter(!like(suggestion, "%TRIG%"))                           # remove Triggers from suggestions
    
    sugs <- sugs %>% mutate(feedbackDate = as.Date(feedbackDate))                         # change field type

    # if reaction to suggestion is not in parameter includeReactions then set the reaction to IGNORE
    sugs <- sugs %>% mutate(reaction = ifelse(!(reaction %in% includeReactions), "Ignored", reaction))

    # for subsequent processing number the records in the new suggestion table
    sugs <- sdf_with_unique_id(sugs, id="recordId")       # just need an id not need to be sequencetial, so use sdf_with_unique_id instead of sdf_with_sequential_id

    sugs <- sugs %>% mutate(dur = 0)
    
    # subset the ignored suggestions
    ign <- sugs %>% filter(reaction=="Ignored")

    if(sdf_dim(ign)[1] > 0)                                                              # if there are any ignored suggestions then find the time from suggestion to next action
    {
        # append the ignored suggestions to the intereactions table (now called events)
        ign <- sdf_bind_rows(ign, events)

        # sort ign table as setup to find time between suggestion and next action
        ign <- arrange(ign, repId, accountId, date)
                        
        # add the dur field to the table that contains that difference in time
        ign <- ign %>% group_by(repId, accountId) %>%
                       mutate(date_lag = lag(date), dur = as.integer(datediff(date, date_lag))) %>% 
                       ungroup() %>% select (-date_lag)        
        
        # move the dur field values to the rows that contain the suggestion associated with the duration  
        ign <- ign %>% mutate(dur = lead(dur)) %>% 
                       mutate(dur = ifelse(is.na(dur), 0, dur))                                     

        # only include the appropriate types of differences
        ign <- ign %>% filter(reaction=="Ignored" & lead(reaction) == "Touchpoint")

        # merge the processed ignored table to the full suggestions table
        sugs <- sugs %>% left_join(select(ign, recordId, dur), by="recordId", suffix = c("_x", "_y"))
        sugs <- sugs %>% select(-dur_x) %>% dplyr::rename(dur = dur_y)

        # if there is no subsequent intereaction after a suggestionthan set the duration to one more than the engageWindow
        sugs <- mutate(sugs, dur = ifelse(is.na(dur), engageWindow+1, dur))

        sugs <- sugs %>% mutate(dur = ifelse(reaction!="Ignored", 0, dur))       # if suggestions are not ignored then set the duration to 0
    }
    
    # setup to count the number of times that suggestions are engaged or not
    # finish that setup
    sugs <- sugs %>% mutate(ctr = 1, include = 0)

    # this is where the suggestions <= engageWindow are included
    sugs <- mutate(sugs, include = ifelse(dur<=engageWindow, 1, 0))
          
    # count the number of suggestions offered and accepted
    sugs <- sugs %>% group_by(repId, accountId) %>% 
                     mutate(yes = sum(include,na.rm=TRUE), total = sum(ctr,na.rm=TRUE)) %>% ungroup() 


    # that ratio is the probability estimate                               
    sugs <- sugs %>% group_by(repId, accountId) %>% 
                     mutate(probAccepted = yes/total) %>% ungroup()

    # prepare for the rep x account non-suggestion engagement estimates
    sugs <- sugs %>% mutate(k = paste(repId, "_", accountId, sep=""))
    flog.info("finish prepare sugs")

    # prepare for the rep x account non-suggestion engagement estimates
    events <- events %>% mutate(k = paste(repId, "_", accountId, sep=""))

 ###### example of sugs at this point in code    
 #        recordId     type                                                   suggestion       date                                                 Title_vod__c repActionTypeId feedbackDate  reaction accountId repId dur
 #     1:        1 Call_vod                           a207764~cp9:VVVV~r2717~V~1:d:p1003 2017-02-20                             SUGGESTED ACTION: Detail  LYRICA               3   2017-02-22  Complete    207764  2717   0
 #     2:        2 Call_vod                              a207764~cp10:~r2717~V~1:d:p1003 2017-06-26                             SUGGESTED ACTION: Detail  LYRICA               3         <NA>   Ignored    207764  2717   1
 #     3:        3 Call_vod                     a209818~cp9:~r2717~V~1:d:p1003:mt1:m1213 2017-01-05 SUGGESTED ACTION: Detail LYR Lyrica DPN August 2015 POA v1.0               3         <NA>   Ignored    209818  2717   6
 #     4:        4 Call_vod                               a209818~cp9:~r2717~V~1:d:p1003 2017-02-01                             SUGGESTED ACTION: Detail  LYRICA               3   2017-03-25 Execution    209818  2717   0
 #     5:        5 Call_vod                              a209818~cp9:V~r2717~V~1:d:p1003 2017-02-15                             SUGGESTED ACTION: Detail  LYRICA               3   2017-02-16 Execution    209818  2717   0
 #    ---                                                                                                                                                                                                                  
 #618933:   618933 Call_vod          a300707~cp11:~r3683~V~1:d:p1001~2:d:p1003~3:d:p1014 2017-08-01            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    300707  3683  61
 #618934:   618934 Call_vod a47737~cp11:~r3683~V~1:d:p1001~2:d:p1003~3:d:p1009~4:d:p1014 2017-07-27   SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX, FLECTOR               3         <NA>   Ignored     47737  3683  61
 #618935:   618935 Call_vod         a189462~cp11:V~r3683~V~1:d:p1003~2:d:p1001~3:d:p1014 2017-08-01            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    189462  3683  61
 #618936:   618936 Call_vod        a192615~cp11:SV~r3683~V~1:d:p1003~2:d:p1001~3:d:p1014 2017-07-18            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    192615  3683  61
 #618937:   618937 Call_vod          a194741~cp11:~r3683~V~1:d:p1003~2:d:p1001~3:d:p1014 2017-07-25            SUGGESTED ACTION: Detail EUCRISA, LYRICA, CHANTIX               3         <NA>   Ignored    194741  3683  61
 ####### end of example
    
    # pick out all unique account rep combinations
    sugs.ar <- sugs %>% filter(date >= sugStartDate) %>% select(k, repId, accountId)
    events.ar <- events %>% select(k, repId, accountId)
    acctReps <- sdf_bind_rows(sugs.ar, events.ar) %>% distinct()
    flog.info("finish prepare acctReps")

    # sort events by rep account date
    events <- arrange(events, k, date)

    # find interevent time differences by rep account
    # Hive functions work inside sparklyr mutate; dplyer or base R function may not work
    events <- events %>% group_by(repId, accountId) %>%
                         mutate(date_lag = lag(date), diff = as.integer(datediff(date, date_lag))) %>%
                         ungroup() %>%
                         select (-date_lag)
    flog.info("finish prepare events")
    
    flog.info("Now prepare for calculation of likelihood in parallel")
    likelihood <- calculateLikelihood(events, acctReps, today, numberCores)

    likelihood <- sdf_copy_to(sc, likelihood, "likelihood", overwrite=TRUE, memory=TRUE, repartition=48)

    flog.info("Now estimates the likelihood of an interaction by day of week and week of month.")

    dayEstimate <- interactions %>% filter(repActionTypeId==chl & date<=today)

    dayEstimate <- calculateDaysEstimate(dayEstimate, today, lookForward) 

    # add the probability that the suggestion was accepted estimated above to the likelihood estimates
    likelihood <- likelihood %>% full_join(sugs %>% select(repId, accountId, probAccepted) %>% distinct(), by=c("repId","accountId"))

    # make sure missing estimates are zero
    likelihood <- likelihood %>% mutate(probTouch = ifelse(is.na(probTouch), 0, probTouch))

    # make small estimates zero 
    likelihood <- likelihood %>% mutate(probTouch = ifelse(probTouch<epsilon, 0, probTouch)) %>% filter(repId > 0)

    # prepare to spread the estimates over the lookForward interval
    likelihood <- addWeekdayWeekmonth(likelihood, today, lookForward)

    likelihood <- likelihood %>% left_join(dayEstimate, by=c("repId","accountId","mw_wd"))
    
    likelihood <- likelihood %>% mutate(ratio = ifelse(is.na(ratio), 0, ratio))

    # finialize the separate estimates of prob touch, prob accept, and the combination     
    likelihood <- likelihood %>% mutate(PT = ratio * probTouch)

    likelihood <- likelihood %>% mutate(probAccepted = ifelse(is.na(probAccepted), 0, probAccepted))

    likelihood <- likelihood %>% mutate(PA = probAccepted)

    likelihood <- likelihood %>% mutate(PB = (PA + PT*(1-PA)))

    flog.info("Get final likelihood.")
    
    tT <- copy(likelihood)                                                            # the next set of code is setup for saving results
 
    tT <- tT %>% mutate(suggestionType = "Target")

    tT <- dplyr::rename(tT, probability = PT)

    tT <- tT %>% select(-c(PA, PB))

    tA <- copy(likelihood)
 
    tA <- tA %>% mutate(suggestionType = "Target")

    tA <- dplyr::rename(tA, probability = PA)

    tA <- tA %>% select(-c(PT, PB))

    tB <- copy(likelihood)

    tB <- tB %>% mutate(suggestionType = "Target")

    tB <- dplyr::rename(tB, probability = PB)

    tB <- tB %>% select(-c(PT, PA))

    if (useForProbability == "T")
        likelihood <- tT %>% distinct()
    else if (useForProbability == "A")
        likelihood <- tA %>% distinct()
    else
        likelihood <- tB %>% distinct()

    likelihood <- likelihood %>% mutate(repActionTypeId = chl, runDate = date_add(today, 1))

    likelihood <- likelihood %>% mutate(learningRunUID = RUN_UID, learningBuildUID = BUILD_UID, repUID = repId, accountUID = accountId)

    likelihood <- likelihood %>% select(learningRunUID,learningBuildUID,repActionTypeId,repUID,accountUID,suggestionType,date,probability)
    
    flog.info("finish calculateEngagement")

    return(likelihood)
}

