library(testthat)

expect_file_exists <- function(object) {
  # Capture object and label
  act <- quasi_label(rlang::enquo(object))
  filepath <- act$val
  # Call expect()
  expect(
    file.exists(filepath),
    sprintf("%s should exists, but not", filepath)
  )
  # Invisibly return the value
  invisible(act$val)
}

expect_file_not_exists <- function(object) {
  # Capture object and label
  act <- quasi_label(rlang::enquo(object))
  filepath <- act$val
  # Call expect()
  expect(
    !file.exists(filepath),
    sprintf("%s should not exists, but exists", filepath)
  )
  # Invisibly return the value
  invisible(act$val)
}

expect_num_of_file <- function(object,n) {
  # Capture object and label
  act <- quasi_label(rlang::enquo(object))
  act$n <- length(list.files(act$val))
  # Call expect()
  expect(
    act$n == n,
    sprintf("Directory:%s should have %i files, not %i", act$val, n, act$n)
  )
  # Invisibly return the value
  invisible(act$val)
}

expect_str_match <- function(object,s,str_start=NULL, str_end=NULL) {
  # Capture object and label
  act <- quasi_label(rlang::enquo(object))
  if (!is.null(str_start) & !is.null(str_end)) {
    act$str <- substr(act$val,str_start,str_end)
  } else if (is.null(str_end)) {
    str_end <- str_start + nchar(s) - 1
    act$str <- substr(act$val,str_start,str_end)
  } else if (is.null(str_start)) {
    str_start <- str_end - nchar(s) + 1
    act$str <- substr(act$val,str_start,str_end)
  }
  else {
    str_start <- ':'
    str_end <- ':'
    act$str <- act$val
  }
  # Call expect()
  expect(
    act$str == s,
    sprintf("String %s [%s,%s] should be '%s', not '%s'", act$lab, str_start, str_end, s, act$str)
  )
  # Invisibly return the value
  invisible(act$val)
}

expect_str_end_match <- function(object, s) {
  str_end <- nchar(object)
  expect_str_match(object,s,str_start=NULL,str_end=str_end)
}

expect_str_start_match <- function(object, s, startOffset=NULL) {
  str_start <- ifelse(is.null(startOffset), 28, 28+startOffset)  # start of log info
  expect_str_match(object,s,str_start=str_start,str_end=NULL)
}

expect_str_pattern_find <- function(object,s) {
  # Capture object and label
  act <- quasi_label(rlang::enquo(object))
  act$find <- regexpr(s,act$val)[1]
  # Call expect()
  expect(
    act$find != -1,
    sprintf("String %s should contain '%s',but not", act$lab,s)
  )
  # Invisibly return the value
  invisible(act$val)
}

expect_str_pattern_notfind <- function(object,s) {
  # Capture object and label
  act <- quasi_label(rlang::enquo(object))
  act$find <- regexpr(s,act$val)[1]
  # Call expect()
  expect(
    act$find == -1,
    sprintf("String %s should not contain '%s',but substr(x,%s,%s) = %s", act$lab,s,act$find,act$find+nchar(s)-1, substr(act$val,act$find,act$find+nchar(s)-1) )
  )
  # Invisibly return the value
  invisible(act$val)
}