import unittest
import sys
import os
import pandas as pd
# import io
import logging

script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
unitTest_dir = os.path.dirname(script_dir)
common_dir = os.path.dirname(unitTest_dir)
sys.path.append(common_dir)

from common.pyUtils.database_utils import DatabasePoolConnection, other_database_op
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig


class DBresetMockData():
    def __init__(self, test_dir, host, user, password, port, dbname, csname):
        self.test_dir = test_dir
        self.dbhost = host
        self.dbuser = user
        self.dbpassword = password
        self.dbport = port
        self.dbname_dse = dbname
        self.dbname_learning = self.dbname_dse + '_learning'
        self.dbname_stage = self.dbname_dse + '_stage'
        self.dbname_cs = csname
        self.db_conn_pool = None
        self.db_map = {"dse": self.dbname_dse, "learning":self.dbname_learning, "stage":self.dbname_stage, "cs":self.dbname_cs}
        self.filename_map = {"dse":"pfizerusdev", "learning":"pfizerusdev_learning", "stage":"pfizerusdev_stage", "cs":"pfizerprod_cs"}

    def connect_to_db(self):
        self.db_conn_pool = DatabasePoolConnection(max_overflow=5, pool_size=2, dbuser=self.dbuser, dbpassword=self.dbpassword, dbhost=self.dbhost, port=self.dbport)

    def dropoldtables(self):
        for schema in self.db_map:
            # print(self.db_map[schema])
            query = "DROP DATABASE IF EXISTS {}".format(self.db_map[schema])
            other_database_op(query, self.db_conn_pool)

    def readcsv(self, schema, table):
        csvpath = self.test_dir + "/data/" + self.filename_map[schema] + "/" + table + ".csv"
        if os.path.isfile(csvpath):
            df = pd.read_csv(csvpath)
            foundDf = True
        else:
            #if there is no local to the module data to override, use the data in common/unitTest
            csvpath = "common/unitTest/data/" + self.filename_map[schema] + "/" + table + ".csv"
            if os.path.isfile(csvpath):
                df = pd.read_csv(csvpath)
                foundDf = True
            else:
                df = ""
                foundDf = False
        return df, foundDf

    def writedata(self, schema, table, csvdf):
        csvdf.to_sql(name=table, con=self.db_conn_pool.get_conn(), schema=self.db_map[schema], if_exists="append", index=False)

    def buildtable(self, schema, table):
        sqlpath = "common/unitTest/dbSetup/" + self.filename_map[schema] + "/" + table + ".sql"
        file = open(sqlpath, "r")
        querylist = file.readlines()[1:]
        # print(query)
        file.close()
        # print(querylist)
        query = "CREATE TABLE " + self.db_map[schema] + "." + table + " (\n"
        for line in querylist:
            if '\n' in line:
                line.replace('\n', '')
            query = query + " " + line
        other_database_op(query, self.db_conn_pool)
        csvdf, foundDf = self.readcsv(schema, table)
        if foundDf == True:
            self.writedata(schema, table, csvdf)

    def close_pool(self):
        self.db_conn_pool.close_all_conn()

    def run(self, requiredDataList={"dse":[], "cs":[], "stage":[], "learning":[], "archive":[]}):
        self.connect_to_db()
        self.dropoldtables()
        query = "SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'only_full_group_by',''));"
        # query = "SET GLOBAL sql_mode = '';"
        other_database_op(query, self.db_conn_pool)
        for schema in self.db_map:
            query = "CREATE DATABASE IF NOT EXISTS {} CHARACTER SET utf8 COLLATE utf8_unicode_ci;".format(self.db_map[schema])
            other_database_op(query, self.db_conn_pool)
            # print("USING: " + self.db_map[schema])
            query = "USE {}".format(self.db_map[schema])
            for table in requiredDataList[schema]:
                # print("     " + table)
                other_database_op(query, self.db_conn_pool)
                self.buildtable(schema, table)
        self.close_pool()
        self.db_conn_pool.get_stat()

    def get_table(self, table, db):
        self.connect_to_db()
        query = "SELECT * FROM " + db + "." + table
        data = pd.read_sql(query, con=self.db_conn_pool.get_engine())
        # data_df.to_sql(table,con=self.db_conn_pool.get_engine(),index=False) #, if_exists='append'
        # returned = other_database_op("SELECT * FROM {DB}.{TABLE}".format(DB=db, TABLE=table), self.db_conn_pool, return_cursor=True)
        # table_returned = other_database_op(query, self)
        self.close_pool()
        return data

    def write_table(self, file, table, schema):
        self.connect_to_db()
        csvpath = self.test_dir + "/references/" + file + ".csv"
        if os.path.isfile(csvpath):
            csvdf = pd.read_csv(csvpath)
            csvdf.to_sql(name=table, con=self.db_conn_pool.get_conn(), schema=self.db_map[schema], if_exists="append", index=False)
        else:
            print("No csv data found for " + str(file))


    def clear(self):
        self.connect_to_db()
        self.dropoldtables()
        self.close_pool()

class unitTestParent(unittest.TestCase):

    def reqDBs(self):
        required_dse = [  ]
        required_cs = [  ]
        required_stage = [  ]
        required_learning = [  ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required

    def setUp(self):
        # initialize DB connection config classs
        self.test_dir = sys.argv[1][9:-1] + "/" + sys.argv[2][9:-1]
        self.db_user = sys.argv[4][8:-1]
        self.db_password = sys.argv[5][12:-1]
        self.db_host = sys.argv[6][8:-1]
        self.dse_db_name = sys.argv[7][8:-1]
        self.db_port = sys.argv[8][5:]  # Update to 33066 for testing on Local machine
        # if len(sys.argv) >= 12:
        self.cs_db_name = sys.argv[10][11:-1]
        # else:
        #
        # Get the singleton instance of the database config and set the properties

        self.learning_db_name =  self.dse_db_name + '_learning'
        self.stage_db_name = self.dse_db_name + '_stage'

        logger = logging.getLogger()
        logger.setLevel(level=logging.DEBUG)
        DBresetMockData(self.test_dir, self.db_host, self.db_user, self.db_password, self.db_port, self.dse_db_name, self.cs_db_name).run(self.reqDBs())

    def tearDown(self):
        # self.text_trap.close()
        # DBresetMockData(self.test_dir, self.db_host, self.db_user, self.db_password, self.db_port, self.dse_db_name).clear()
        pass

    def get_table(self, table, db):
        table_returned = DBresetMockData(self.test_dir, self.db_host, self.db_user, self.db_password, self.db_port, self.dse_db_name, self.cs_db_name).get_table(table, db)
        return table_returned

    def write_table(self, file, table, db):
        DBresetMockData(self.test_dir, self.db_host, self.db_user, self.db_password, self.db_port, self.dse_db_name, self.cs_db_name).write_table(file, table, db)
        # return table_returned
