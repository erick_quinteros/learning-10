CREATE TABLE `InteractionProductMessage` (
  `interactionId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `messageId` int(11) NOT NULL,
  `productInteractionTypeId` tinyint(4) NOT NULL,
  `messageReaction` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matchedSuggestionMessage` tinyint(1) DEFAULT NULL,
  `physicalMessageUID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `messageOrder` int(11) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Soft delete flag.',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`interactionId`,`productId`,`messageId`,`productInteractionTypeId`),
  KEY `interactionproductmessage_interactionId` (`interactionId`),
  KEY `interactionproductmessage_productId` (`productId`),
  KEY `interactionproductmessage_messageId` (`messageId`),
  KEY `interactionproductmessage_productInteractionTypeId` (`productInteractionTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci