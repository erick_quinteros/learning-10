INFO [2018-10-07 00:24:28] Run directory = /Users/jiajingxu/Documents/learning/bin/../builds/unit-test-anchor
INFO [2018-10-07 00:24:28] Run initialized at: 2018-10-07 00:24:28
INFO [2018-10-07 00:24:28] Running Anchor manually: has defined predicutRundate in learning.properties, running with predictRundate=2017-11-01
INFO [2018-10-07 00:24:29] PredictAhead=5: predict for 2017-11-01,2017-11-02,2017-11-03,2017-11-06,2017-11-07,2017-11-08
INFO [2018-10-07 00:24:29] Start Loading Anchor Data
INFO [2018-10-07 00:24:29] finish read Interaction Data, dim=(5804,7)
INFO [2018-10-07 00:24:29] planned future events nrows=24
INFO [2018-10-07 00:24:29] loaded account dim=(24,2) as there are planned visits
INFO [2018-10-07 00:24:29] subset with date/isCompleted interactions dim= (5318,6)
INFO [2018-10-07 00:24:29] return form loadAnchorData
INFO [2018-10-07 00:24:29] Start calcuateAnchor
INFO [2018-10-07 00:24:29] calcualte day average latitude and longitude
INFO [2018-10-07 00:24:29] start parellel processing
INFO [2018-10-07 00:24:29] Core 1: Start processing for repId=1505
INFO [2018-10-07 00:24:29] Core 2: Start processing for repId=1605
INFO [2018-10-07 00:24:30] Core 1: repId=1505 avgMaxDailyTravel=142218.237409424, withinDayDistance=7888.09797491781
INFO [2018-10-07 00:24:30] Core 1: repId=1505 has 24 planned visit in the predict ahead time frame
INFO [2018-10-07 00:24:30] Core 2: repId=1605 avgMaxDailyTravel=61605.5310902329, withinDayDistance=26776.38898494
INFO [2018-10-07 00:24:30] Core 2: repId=1605 finished clustering got 12 centers
INFO [2018-10-07 00:24:30] Core 2: repId=1605 after addind dayOfWeek, cluster centers = 45
INFO [2018-10-07 00:24:30] Core 2: repId=1605: finish fitting markov chain, start looping to predict
INFO [2018-10-07 00:24:30] Core 2: repId=1605 finish prediction, predicted cluster center:4_Wednesday->2_Thursday->2_Friday->3_Monday->6_Tuesday->4_Wednesday
INFO [2018-10-07 00:24:30] Core 2: finish processing, dim(result)=(288,11)
INFO [2018-10-07 00:24:32] Core 1: repId=1505 finished clustering got 37 centers
INFO [2018-10-07 00:24:32] Core 1: repId=1505 after addind dayOfWeek, cluster centers = 83
INFO [2018-10-07 00:24:32] Core 1: repId=1505: finish fitting markov chain, start looping to predict
INFO [2018-10-07 00:24:32] Core 1: repId=1505 finish prediction, predicted cluster center:18_Wednesday->25_Thursday->26_Friday->11_Monday->1_Tuesday->18_Wednesday
INFO [2018-10-07 00:24:32] Core 1: finish processing, dim(result)=(440,11)
INFO [2018-10-07 00:24:32] return from calcualteAnchor
INFO [2018-10-07 00:24:32] Enter process anchor result dim=(728,11)
INFO [2018-10-07 00:24:32] Process anchor result dim=(728,11) using Centroid mode
INFO [2018-10-07 00:24:32] Return from Centroid Mode, dim(resultProcessed)=(10,11)
INFO [2018-10-07 00:24:32] Process anchor result dim=(728,11) using Facility mode
INFO [2018-10-07 00:24:32] Return from Facility Mode, dim(resultProcessed)=(330,11)
INFO [2018-10-07 00:24:32] Adding results from predictMode:Facility to the default Centroid mode results
INFO [2018-10-07 00:24:32] Return from processAnchorResult with dim(resultProcessed)=(340,11)
INFO [2018-10-07 00:24:32] Enter Save Anchor Result，save 340 row of results to DB
INFO [2018-10-07 00:24:32] return from saveAnchorResult
INFO [2018-10-07 00:24:32] Finished running Anchor model
