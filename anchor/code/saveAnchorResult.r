##########################################################
#
#
# aktana-learning Anchor estimates Aktana Learning Engines.
#
# description: save anchor results
#
#
# created by : marc.cohen@aktana.com
# updated by : shirley.xu@aktana.com
#
# created on : 2015-10-27
# updated on : 2018-07-31
#
# Copyright AKTANA (c) 2015.
#
#
####################################################################################################################
library(futile.logger)
library(Learning.DataAccessLayer)

saveAnchorResult.RepDateLocation <- function(isNightly, resultCentroid, RUN_UID, BUILD_UID) {
  
  # addd runUID
  resultCentroid$learningRunUID <- RUN_UID
  
  # save result to DB
  if (isNightly) {
    # drop column not needed
    resultCentroid[,createdAt:=runDate]
    resultCentroid[,c("probability","runDate"):=NULL]

    # delete old data

    dataAccessLayer.anchor.truncate.RepDateLocation()
    # write result back to DB
    # write back to table (overwrite)
    flog.info("save %s rows of RepDateLocation to DSE", dim(resultCentroid)[1])
    dataAccessLayer.anchor.write.RepDateLocation_dse(resultCentroid)
  } else { # manual running
    # add learningBuildUID
    resultCentroid$learningBuildUID <- BUILD_UID
    # delete old data first
    dataAccessLayer.anchor.deleteFrom.RepDateLocation(BUILD_UID)  # remove old build records in Learning DB
    # write result back to DB
    flog.info("save %s rows of RepDateLocation to Learning", dim(resultCentroid)[1])
    dataAccessLayer.anchor.write.RepDateLocation_l(resultCentroid)
  }
}

saveAnchorResult.RepDateFacility <- function(isNightly, resultFacility, RUN_UID, BUILD_UID) {

  # addd runUID
  resultFacility$learningRunUID <- RUN_UID
  
  # save result to DB
  if (isNightly) {
    # drop column not needed
    resultFacility[,createdAt:=runDate]
    resultFacility[,c("runDate","accountId"):=NULL]
  
    # delete old data
    dataAccessLayer.anchor.truncate.RepDateFacility() # write result back to DB
    # write back to table (overwrite)
    flog.info("save %s rows of RepDateFacility to DSE", dim(resultFacility)[1])
    dataAccessLayer.anchor.write.RepDateFacility_dse(resultFacility)
  } else { # manual running
    # add learningBuildUID
    resultFacility$learningBuildUID <- BUILD_UID
    # delete old data first
    dataAccessLayer.anchor.deleteFrom.RepDateFacility(BUILD_UID)  # remove old build records in Learning DB
    # write result back to DB
    flog.info("save %s rows of RepDateFacility to Learning", dim(resultFacility)[1])
    dataAccessLayer.anchor.write.RepDateFacility_l(resultFacility)
  }
}

####################################
### Main Func
####################################
saveAnchorResult <- function(isNightly, resultList, RUN_UID, BUILD_UID) {
  
  flog.info("Enter Save Anchor Result")
  
  # save result to DB
  saveAnchorResult.RepDateLocation(isNightly, resultList[["resultCentroid"]], RUN_UID, BUILD_UID)
  if(!is.null(resultList[["resultFacility"]])) {
    saveAnchorResult.RepDateFacility(isNightly, resultList[["resultFacility"]], RUN_UID, BUILD_UID)
  }
  
  flog.info('Return from saveAnchorResult')
  
}
 