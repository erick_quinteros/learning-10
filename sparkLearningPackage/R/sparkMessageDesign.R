##########################################################
##
##
## aktana-learning Install Aktana Learning Engines.
##
## description: Driver Code
## 1. analyze message opens/click likelihoods
##
## created by : marc.cohen@aktana.com
##
## created on : 2015-11-03
##
## Copyright AKTANA (c) 2015.
##
##
##########################################################

### if there is a single sendName and targetName

sparkMessageDesign <- function (ints, sendName, targetName) 
{
  # input:
  # ints <- data.frame(accountId=c(1616,1653,1653,1653,1653), date=c(as.Date("2016-07-27"), as.Date("2016-03-29"), as.Date("2016-03-29"), as.Date("2016-07-26"), as.Date("2016-07-26")), key=c("SEND...a3RA0000000e486MAA","SEND...a3RA0000000e47gMAA","SEND...a3RA0000000e486MAA","OPEN...a3RA0000000e486MAA","SEND...a3RA0000000e486MAA"), priorVisit=c(-1,3,5,-1,-1))
  # productName physicalMessageUID  repId   accountId productInteractionTypeName  date       key                     priorVisit  actionOrder
  #   CHANTIX   a3RA0000000e486MAA   1615      1616           SEND             2016-07-27  SEND...a3RA0000000e486MAA     -1           1
  #   CHANTIX   a3RA0000000e47gMAA   1307      1653           SEND             2016-03-09  SEND...a3RA0000000e47gMAA      3           1
  #   CHANTIX   a3RA0000000e486MAA   1307      1653           SEND             2016-03-09  SEND...a3RA0000000e486MAA      5           1
  #   CHANTIX   a3RA0000000e486MAA   1307      1653           RTE_OPEN         2016-07-26  OPEN...a3RA0000000e486MAA     -1           2
  #   CHANTIX   a3RA0000000e486MAA   1307      1653           RTE_CLICK         2016-07-26  OPEN...a3RA0000000e486MAA     -1          3
  #   CHANTIX   a3RA0000000e486MAA   1307      1653           SEND             2016-07-26  SEND...a3RA0000000e486MAA     -1           1
  # sendName <- "SEND...a3RA0000000e486MAA"
  # targetName <- "OPEN...a3RA0000000e486MAA"
  # 
  # expected output:
  #   accountId OPEN...a3RA0000000e486MAA priorVisit SEND...a3RA0000000e47gMAA SEND...a3RA0000000e486MAA
  #     1616                         0         -1                         0                         1
  #     1653                         1          5                         1                         1
  
  # identify accounts that have either send or target
  accts <- ints %>% 
    filter(key %in% c(sendName,targetName)) %>%
    pull(accountId) %>%
    unique()
  flog.info("Number of accounts considered for dynamic features: %s", length(accts))
  
  # include only touchpoint history for those accounts 
  ints <- ints %>%
    filter(accountId %in% accts) %>%
    distinct() %>%
    arrange(accountId, date, actionOrder, physicalMessageUID) %>%
    select(accountId,date,key,priorVisit)

  t <- EncodeMessages(ints, targetName, sendName)
  t <- unique(t)
  # t$measure <- 1
  t <- sdf_copy_to(spark_connection(ints), t, "t", overwrite=TRUE, memory=FALSE) %>%
    sdf_pivot(recordId+accountId~value, fun.aggregate=list(measure="sum")) %>%          # dcast
    select(-recordId) %>%
    mutate(priorVisit=ifelse(is.na(priorVisit),-1,priorVisit)) %>%                      # fill NA
    na.replace(0)
    
  return(t)
}
