import unittest
from unittest.mock import patch, MagicMock
import sys
import os
import pandas as pd
import io

script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
module_dir = os.path.dirname(script_dir)
learning_dir = os.path.dirname(module_dir)
sys.path.append(learning_dir)

from common.unitTest.pyunittestUtils import unitTestParent
from simulationMessageSequence.SimulationStarter import get_mso_build_uid_list, read_learning_properties, get_copy_storm_database_name, start_simulation, populate_database_tables, main
from simulationMessageSequence.LearningPropertiesReader import LearningPropertiesReader


class test_SimulationStarter(unitTestParent):

    def setUp(self):
        super().setUp()
        self.learning_prop_file_path = os.path.join(self.test_dir, "data", "builds", "unit-test-mso-build", "learning.properties")

    def tearDown(self):
        super().tearDown()

class test_get_mso_build_uid_list(test_SimulationStarter):
#if not implementing a file parent function like test_[FILE], have each test class inherit from unitTestParent

    @patch("common.DataAccessLayer.DataAccessLayer.DSEAccessor.get_active_undeleted_message_algorithm_externalIds")
    def test_empty_table(self, mock_get_active_undeleted_message_algorithm_externalIds):
        #function inputs
        mock_get_active_undeleted_message_algorithm_externalIds.return_value = pd.DataFrame()

        #expected outputs
        reference_mso_build_uids = []

        #testing outputs
        mso_build_uids = get_mso_build_uid_list()
        self.assertEqual(reference_mso_build_uids, mso_build_uids)

    @patch("common.DataAccessLayer.DataAccessLayer.DSEAccessor.get_active_undeleted_message_algorithm_externalIds")
    @patch("os.path.join")
    @patch("simulationMessageSequence.SimulationStarter.LearningPropertiesReader")
    def test_populated_table(self, mock_LearningPropertiesReader, mock_join, mock_get_active_undeleted_message_algorithm_externalIds):
        #function inputs
        mock_get_active_undeleted_message_algorithm_externalIds.return_value = pd.DataFrame({"a":["b"]})
        mock_join.return_value = self.learning_prop_file_path
        mock_LearningPropertiesReader.return_value.get_property.return_value = "test"

        #expected outputs
        reference_mso_build_uids = ["test"]

        #testing outputs
        mso_build_uids = get_mso_build_uid_list()
        self.assertEqual(reference_mso_build_uids, mso_build_uids)
        mock_get_active_undeleted_message_algorithm_externalIds.assert_called()
        mock_join.assert_called()
        mock_LearningPropertiesReader.assert_called()

class test_read_learning_properties(test_SimulationStarter):
#if not implementing a file parent function like test_[FILE], have each test class inherit from unitTestParent

    @patch("os.path.isfile")
    def test_assert_fail(self, mock_isfile):
        #function inputs
        mock_isfile.return_value = False

        #expected outputs
        #testing outputs
        read_learning_properties("test")
        self.assertRaises(AssertionError, read_learning_properties, "")

    @patch("os.path.isfile")
    @patch("simulationMessageSequence.SimulationStarter.LearningPropertiesReader")
    def test_assert_success(self, mock_LearningPropertiesReader, mock_isfile):
        #function inputs
        mock_isfile.return_value = True

        #expected outputs
        #testing outputs
        learning_prop_config = read_learning_properties("test")
        self.assertIsInstance(learning_prop_config, MagicMock)

class test_get_copy_storm_database_name(test_SimulationStarter):
#if not implementing a file parent function like test_[FILE], have each test class inherit from unitTestParent

    def test_pfizerus(self):
        #function inputs
        dse_db_name = "pfizerus"

        #expected outputs
        referencecs_db_name = "pfizerprod_cs"

        #testing outputs
        cs_db_name = get_copy_storm_database_name(dse_db_name)
        self.assertEqual(referencecs_db_name, cs_db_name)

    def test_misc_db_name(self):
        #function inputs
        dse_db_name = "test"

        #expected outputs
        referencecs_db_name = "test_cs"

        #testing outputs
        cs_db_name = get_copy_storm_database_name(dse_db_name)
        self.assertEqual(referencecs_db_name, cs_db_name)

class test_start_simulation(test_SimulationStarter):
#if not implementing a file parent function like test_[FILE], have each test class inherit from unitTestParent

    @patch("simulationMessageSequence.SimulationStarter.SimulationMessageSequenceDriver")
    def test_correct_calls(self, mock_SimulationMessageSequenceDriver):
        #function inputs
        #expected outputs
        #testing outputs
        start_simulation("test")
        mock_SimulationMessageSequenceDriver.assert_called()
        mock_SimulationMessageSequenceDriver().start_simulation.assert_called()

class test_populate_database_tables(test_SimulationStarter):
#if not implementing a file parent function like test_[FILE], have each test class inherit from unitTestParent

    @patch("simulationMessageSequence.SimulationStarter.DatabaseIntializer")
    def test_correct_calls(self, mock_DatabaseIntializer):
        #function inputs
        #expected outputs
        #testing outputs
        populate_database_tables()
        mock_DatabaseIntializer.assert_called()
        mock_DatabaseIntializer().populate_email_data_all_products.assert_called()
        mock_DatabaseIntializer().populate_approved_documents_data_global.assert_called()
        return

class test_main(test_SimulationStarter):
#if not implementing a file parent function like test_[FILE], have each test class inherit from unitTestParent

    @patch("simulationMessageSequence.SimulationStarter.len")
    @patch("simulationMessageSequence.SimulationStarter.DatabaseConfig")
    @patch("simulationMessageSequence.SimulationStarter.get_mso_build_uid_list")
    @patch("simulationMessageSequence.SimulationStarter.mp")
    def test_too_few_args(self, mock_mp, mock_get_mso_build_uid_list, mock_DatabaseConfig, mock_len):
        #function inputs
        mock_len.return_value = 5

        #expected outputs
        printedOutput = io.StringIO()
        sys.stdout = printedOutput

        #testing outputs
        main()
        sys.stdout = sys.__stdout__
        self.assertEqual("Invalid arguments to the simulation script\n", printedOutput.getvalue())

    @patch("simulationMessageSequence.SimulationStarter.len")
    @patch("simulationMessageSequence.SimulationStarter.DatabaseConfig")
    @patch("simulationMessageSequence.SimulationStarter.get_mso_build_uid_list")
    @patch("simulationMessageSequence.SimulationStarter.mp")
    def test_too_many_args(self, mock_mp, mock_get_mso_build_uid_list, mock_DatabaseConfig, mock_len):
        #function inputs
        mock_len.return_value = 7

        #expected outputs
        printedOutput = io.StringIO()
        sys.stdout = printedOutput

        #testing outputs
        main()
        sys.stdout = sys.__stdout__
        self.assertEqual("Invalid arguments to the simulation script\n", printedOutput.getvalue())


    @patch("simulationMessageSequence.SimulationStarter.len")
    @patch("simulationMessageSequence.SimulationStarter.get_copy_storm_database_name")
    @patch("simulationMessageSequence.SimulationStarter.DatabaseConfig")
    @patch("simulationMessageSequence.SimulationStarter.get_mso_build_uid_list")
    @patch("simulationMessageSequence.SimulationStarter.mp")
    def test_expected_arg(self, mock_mp, mock_get_mso_build_uid_list, mock_DatabaseConfig, mock_get_copy_storm_database_name, mock_len):
        #function inputs
        mock_len.return_value = 6

        #expected outputs
        #testing outputs
        main()
        mock_len.assert_called()
        mock_get_copy_storm_database_name.assert_called()
        mock_DatabaseConfig.instance.assert_called()
        mock_get_mso_build_uid_list.assert_called()
        mock_mp.Pool.assert_called()
