
class Utils:
    def __init__(self, glue_context, customer):
        self.glueContext = glue_context
        self.customer = customer
        pass

    def df_to_partitions(self,df, partition_columns_list, s3_destination):
        '''
        :param df:
        :param partition_columns_list:
        :param s3_destination:
        :return:
        '''
        df.repartition(*partition_columns_list).write.mode('overwrite').partitionBy(*partition_columns_list).parquet(
            s3_destination)
        return True

    def df_to_partitions_append(self,df, partition_columns_list, s3_destination):
        '''

        :param df:
        :param partition_columns_list:
        :param s3_destination:
        :return:
        '''

        df.repartition(*partition_columns_list).write.mode('append').partitionBy(*partition_columns_list).parquet(
            s3_destination)
        return True

    # storedfwithunpartition
    def df_to_unpartition(self,df, s3_destination):
        '''

        :param df:
        :param s3_destination:
        :return:
        '''
        df.write.mode('overwrite').parquet(s3_destination)
        return True

    def data_load_parquet(self,glueContext, s3_load_parquet):
        '''

        :param glueContext:
        :param s3_load_parquet:
        :return:
        '''
        # read the dataloading sript and convert it to dynamic data frame. It's a pipe delimited csv file with partitions
        dynamic_frame0 = glueContext.create_dynamic_frame_from_options('s3', connection_options={'paths': [s3_load_parquet],
                                                                                                 "partitionKeys": [
                                                                                                     'InteractionYear']},
                                                                       format="parquet",
                                                                       transformation_ctx="dynamic_frame0")
        # convert dynamic dataframe to pandas dataframe for faster operation
        df = dynamic_frame0.toDF()
        return df

    # load the csv scrip by sending glue context and path to csv file[Pipe delimited, with headers]. returns pandas dataframe
    def data_load_csv(self,glueContext, s3_load_csv):
        '''

        :param glueContext:
        :param s3_load_csv:
        :return:
        '''
        # read the dataloading sript and convert it to dynamic data frame. It's a pipe delimited csv file with partitions
        dynamic_frame0 = glueContext.create_dynamic_frame_from_options('s3', connection_options={'paths': [s3_load_csv], },
                                                                       format_options={"withHeader": True,
                                                                                       "separator": "|"}, format="csv",
                                                                       transformation_ctx="dynamic_frame0")
        # convert dynamic dataframe to pandas dataframe for faster operation
        df = dynamic_frame0.toDF().toPandas()
        return df

    def df_topartition_delta(self,df, partition, location):
        '''

        :param df:
        :param partition:
        :param location:
        :return:
        '''
        df.write.format("delta").partitionBy(partition).save(location)

    def df_to_delta(self, df, location):
        '''

        :param df:
        :param location:
        :return:
        '''
        df.write.format("delta").mode("overwrite").save(location)